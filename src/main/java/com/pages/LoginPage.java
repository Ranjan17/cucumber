package com.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage {
	
	private WebDriver driver;
	
	//1. By locators
	private By email = By.id("email");
	private By password = By.id("passwd");
	private By signInButton = By.id("SubmitLOgin");
	private By forgotPasswordLink = By.linkText("Forgot your password?");
	
	//2.Constructor
	public LoginPage(WebDriver driver) {
		this.driver = driver;
	}
	
	//3. Page actions
	
	public String getLoginPageTitle() {
		return driver.getTitle();
	}
	
	public boolean isForgotPasswordLinkExist() {
		return driver.findElement(forgotPasswordLink).isDisplayed();
		
	}
	
	public void enterUserName(String userName) {
		driver.findElement(email).clear();
		driver.findElement(email).sendKeys(userName);
	}
	
	public void enterPassword(String pwd) {
		driver.findElement(password).clear();
		driver.findElement(password).sendKeys(pwd);
	}
	
	public void clickOnLogin() {
		driver.findElement(signInButton).click();
	}
	
	public AccountsPage doLogin(String un, String pwd) {
		System.out.println("login with: " + un + " and " + pwd);
		driver.findElement(email).sendKeys(un);
		driver.findElement(password).sendKeys(pwd);
		driver.findElement(signInButton).click();
		return new AccountsPage(driver);
	}
	

}
